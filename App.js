import React from 'react';
import {createAppContainer} from 'react-navigation';
import routes from './src/config/routes';
import {createStackNavigator} from 'react-navigation-stack';
import {initApi} from './src/config/services/api';

const route = createStackNavigator(routes);

// export default createAppContainer(route);

// const AppNavigator = createStackNavigator(routes);

const AppNavigator = createAppContainer(route);

export default class extends React.Component {
  componentWillMount() {
    initApi();
  }
  render() {
    return <AppNavigator />;
  }
}
