/* eslint-disable prettier/prettier */
import React from 'react';
import {View, Text,Keyboard,ImageBackground, StyleSheet, FlatList, TextInput, Button } from 'react-native';
import { getMessages, postMessage } from '../config/services/api';
import Message from '../Messages'
import Compose from '../Compose'
export default class ChatScreen extends React.Component {
    constructor(props) {
        super(props);
        this.submit = this.submit.bind(this);
    }
    static navigationOptions = {
        title: 'Chat Screenaa  ddd!',
    };
    state = {
        messages: [],
        arrt:[],
        text:'',
        stat:'teacher',
        contact:[]
    }
    
    componentDidMount() {
        try {
            console.log('contackt niih = '+JSON.stringify(this.props.navigation.getParam('item', 'kosong')))
            // var tamp = []
            // tamp = this.props.navigation.getParam('item', 'kosong')
            this.setState({contact:this.props.navigation.getParam('item', [])},()=>{
                try {
                    if (this.state.contact[0].status==='parent'){
                        this.setState({stat:'parent'})
                    }    
                } catch (error) {
                    
                }
                
            
            })
            this.unsubscribeGetMessages = getMessages((snapshot) => {
                console.log('message atas = '+JSON.stringify(snapshot.val()))
                
                this.setState({
                    messages: snapshot.val()
                },()=>{
                    const{messages} = this.state
                    var obj = []

                    const fbObject = snapshot.val();
                    const newArr = Object.keys(fbObject).map((key) => {
                    fbObject[key].id = key;
                    return fbObject[key];
                    });

                    newArr.map((data, index) => {
                        // console.log("dataaa "+index+" = "+JSON.stringify(data))
                        console.log("contact "+index+" = "+JSON.stringify(this.state.contact))
                        try {
                            
                            if (data.teacherID===1&&data.parentID===this.state.contact.parentID){
                                obj.push(data)
                                console.log("dataaa messf "+JSON.stringify(data))
                            }
                            if (data.teacherID===1&&data.parentID===this.state.contact[0].parentID){
                                obj.push(data)
                                console.log("dataaa messf "+JSON.stringify(data))
                            }    
                        } catch (error) {
                            
                        }
                        
                        
                    })
                    console.log("dataaa newrr "+JSON.stringify(obj))
                    this.setState({arrt:obj})

                    
                })
            })    
        } catch (error) {
            console.log("erorrr = "+error)
        }
        
    }
    componentWillUnmount() {
        // this.unsubscribeGetMessages();
    }
    
    submit() {
        try {
            if (this.state.stat==='parent'){
                postMessage(this.state.text, this.state.contact[0].parentID, this.state.contact[0].teacherID, 'parent');
            }else{
                console.log('Masuk Kondisi teacher == '+JSON.stringify(this.state.contact))
                postMessage(this.state.text, this.state.contact.parentID, this.state.contact.teacherID, 'teacher');
            }
        } catch (error) {
            
        }

        this.setState({
            text: '',
        });
        
        Keyboard.dismiss();
    }


    GridView1({ item }) {
        try {
            console.log('pesan aku kampang  = '+JSON.stringify(item))    
        } catch (error) {
            
        }
        
        return (
            <View style={[styles.message, item.status===this.state.stat && styles.incomingMessage]}>
                <Text>{item.chat}</Text>
            </View>
        );
    }
    render() {
        return (
            <ImageBackground
                style={[ styles.container, styles.backgroundImage ]}
                source={require('../assets/background.png')}>
                {<FlatList
                    style={styles.container}
                    data={this.state.arrt.reverse()}
                    renderItem={this.GridView1.bind(this)}
                    // keyExtractor={(item, index) => (`message-${index}`)}
                    inverted={-1}

                />}
                
                <View style={styles.compose}>
                <TextInput
                    style={styles.composeText}
                    value={this.state.text}
                    onChangeText={text => this.setState({text})}
                    onSubmitEditing={event => this.submit()}
                    editable={true}
                    maxLength={40}
                />
                <Button
                //ketika tombol ditekan maka akan memanggil method submit
                onPress={this.submit}
                title="Send"
                />
            </View>
            </ImageBackground>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'transparent'
    },
    message: {
        width: '70%',
        margin: 10,
        padding: 10,
        backgroundColor: 'white',
        borderColor: '#979797',
        borderStyle: 'solid',
        borderWidth: 1,
        borderRadius: 10,
      },
      incomingMessage: {
        alignSelf: 'flex-end',
        backgroundColor: '#E1FFC7',
      },
    listItem: {
        width: '70%',
        margin: 10,
        padding: 10,
        backgroundColor: 'white',
        borderColor: '#979797',
        borderStyle: 'solid',
        borderWidth: 1,
        borderRadius: 10
    },
    composeText: {
        width: '80%',
        paddingHorizontal: 10,
        height: 40,
        backgroundColor: 'white',
        borderColor: '#979797',
        borderStyle: 'solid',
        borderWidth: 1,
      },
      compose: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        margin: 10,
      },
    
});
