/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  ActivityIndicator,
  Image,
  TouchableOpacity,
  Alert,
  ScrollView,
  StyleSheet,
  AsyncStorage,
  Text,
  Button,
  View,
} from 'react-native';
import {Scale} from '../assets/index';
import {Input} from '../Components/InputCardComponent';
export default class PenilaianScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static navigationOptions = ({navigation}) => ({
    title: 'Penilaian Siswa',
    headerStyle: {
      backgroundColor: 'white',
    },
    headerTintColor: 'black',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  });

  componentDidMount() {}

  render() {
    return (
      <View style={s.container}>
        <ScrollView>
          <View style={{paddingTop: Scale(10)}}>
            <Input
              label="Player Nickname"
              // value={this.state.Users.nickName}
              // onChangeText={text =>
              //   this.setState(prevState => ({
              //     Users: {
              //       ...prevState.Users,
              //       nickName: text,
              //     },
              //   }))
              // }
              // error={this.state.nickName_error}
              statusError={false}
              // borderColor={this.state.nickName_error ? 'red' : 'white'}
              pdg={0}
              style={{height: 40}}
              // autoFocus={this.state.nickName_error ? true : false}
            />
            <Input
              label="Player Email"
              // onChangeText={text =>
              //   this.setState(prevState => ({
              //     Users: {
              //       ...prevState.Users,
              //       emailUser: text,
              //     },
              //   }))
              // }
              // value={this.state.Users.emailUser}
              // error={this.state.emailUser_error}
              statusError={false}
              // borderColor={this.state.emailUser_error ? 'red' : 'white'}
              pdg={0}
              style={{height: 40}}
            />

            <Input
              label="Phone Number"
              // onChangeText={text =>
              //   this.setState(prevState => ({
              //     Users: {
              //       ...prevState.Users,
              //       phoneNumber: text,
              //     },
              //   }))
              // }
              // value={this.state.Users.phoneNumber}
              // error={this.state.phoneNumber_error}
              statusError={false}
              // borderColor={this.state.phoneNumber_error ? 'red' : 'white'}
              pdg={0}
              style={{height: 40}}
              keyboardType="number-pad"
            />

            <Button
              onPress={() => this.action()}
              title={'Register'}
              color="#841584"
              accessibilityLabel="Register"
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

const s = StyleSheet.create({
  container: {
    flex: 1,
    padding: 15,
    backgroundColor: 'white',
  },
  frame: {
    flexDirection: 'row',
    borderRadius: 5,
    backgroundColor: '#c6c6c6',
    margin: 10,
    padding: 3,
  },
});
