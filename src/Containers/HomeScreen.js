/* eslint-disable quotes */
/* eslint-disable prettier/prettier */
import React from 'react';
import {AsyncStorage,FlatList, View, Button,Alert,Text, StyleSheet, TouchableOpacity, ImageBackground } from 'react-native';
import { getMessages, postAbsen, getData, updateAbsen } from '../config/services/api';
import {Scale} from '../assets/index'
import Icon from 'react-native-vector-icons/Entypo';
import Icon1 from 'react-native-vector-icons/MaterialCommunityIcons';
import {SearchBar} from '../Components/SearchBar'
import {SiswaCard} from '../Components/SiswaCard'
import Modal from 'react-native-modal';
import { Tooltip } from 'react-native-elements';

export default class HomeScreen extends React.Component {
    static navigationOptions = {
      header:null,
        title: 'Home Screen!!',
      };
      state = {
        absen:'',
        siswa: [{},{},{}],
        data1:[],
        value:0,
        visibleModal: false,
        curTime:null,
        data:[
          {
          nama:''
        }],
        login:'Login'
    }
    componentDidMount() {

      this.unsubscribeGetMessages = getData('student',(snapshot) => {
        console.log('message atas = '+JSON.stringify(snapshot.val()))
        console.log('tanggaalll = '+JSON.stringify(this.state.curTime))
        var res = []
        const fbObject = snapshot.val();
        const newArr = Object.keys(fbObject).map((key) => {
        fbObject[key].id = key;
        return fbObject[key];
        });
        res = snapshot.val()
        console.log('Users = '+JSON.stringify(newArr))
        var obj=[]
        let stat = false
        try {
            newArr.map((data, index)=>{
                console.log('data ss= '+JSON.stringify(data))
                obj.push(data)
            })
            this.setState({siswa:obj})
        } catch (error) {
            console.log(error)
        }
    })
    var date = new Date().getDate(); //Current Date
    var month = new Date().getMonth() + 1; //Current Month
    var year = new Date().getFullYear(); //Current Year

      this.setState({
        curTime : date +" "+month+" "+year
      })
      const {navigation} = this.props;
      navigation.addListener ('willFocus', () =>{
        console.log('masuk didount')
      var value = []
      value = AsyncStorage.getItem('Users');
      value.then((e)=>{
        console.log('dat e')
        console.log(e)
        if (e){
        this.setState({data:JSON.parse(e), login:'Logout'},()=>{
          console.log('DATA USER pa eko  = '+JSON.stringify(this.state.data))
          
        })}else{
          this.props.navigation.navigate('LoginScreen')
        }
      },()=>{
      })
    })

  }


  gotoLogin(){
      console.log('masuk elae')
      this.props.navigation.navigate('LoginScreen')
      AsyncStorage.removeItem('Users')
  }

  _renderModalContent = () => {
  return (
    <View style={styles.modalContent}>
      <Text>Absen {this.state.data1.nama} Sekarang ?</Text>
      <View style={{flexDirection:'row',padding:Scale(15), justifyContent:'space-between'}}>
          {this._renderButton('hadir', 'green')}
          <View style={{width:5}}/>
          {this._renderButton('izin', 'blue')}
      </View>
      <View style={{flexDirection:'row', justifyContent:'space-between',padding:Scale(15), paddingTop:Scale(-5)}}>
          <View style={{width:5}}/>
          {this._renderButton('alfa',  'red')}
          <View style={{width:5}}/>
          {this._renderButton('sakit', 'yellow')}
      </View>
      <View style={{right:Scale(-100)}}>
      {this._renderButton('nanti', 'grey')}
      </View>
    </View>
  );}

  _renderButton = (text, color) => (
    <TouchableOpacity onPress={()=> this.setState({ visibleModal: null, absen:text },()=>{
      // console.log('absen on arender = '+JSON.stringify(this.state.data1))
      const fbObject = this.state.data1.absen;
      var newArr = []
      try {
        const fbObject = this.state.data1.absen;
          newArr = Object.keys(fbObject).map((key) => {
          fbObject[key].id = key;
          return fbObject[key];
          });  
      } catch (error) {
        
      }
      console.log('newArr = '+JSON.stringify(newArr))
      var tamp =[]
      newArr.map((data, index)=>{
          tamp = data
      })
      console.log('arr tamp = '+JSON.stringify(tamp))
      if(tamp.tanggal===this.state.curTime) updateAbsen(this.state.data1.id, tamp.id, text)
      else postAbsen(this.state.data1.id, this.state.absen, this.state.curTime)

    })}>
      <View style={[styles.regis,{backgroundColor:color, width: text==='nanti'?Scale(30):Scale(70)}]}>
        <Text style={{color:'white', fontWeight:'bold'}}>{text}</Text>
      </View>
    </TouchableOpacity>
  );

  onAbsen=(data)=>{
    try {
      // if(text!=='Nanti')this.setState({ visibleModal: null, absen:text },()=>{console.log('absen on = '+this.state.absen)})
      // else this.setState({ visibleModal: null })
      this.setState({ visibleModal: !this.state.visibleModal, data1:data })
      // console.log('data index = '+JSON.stringify(data))
      // console.log(data)
    } catch (error) {
      console.log("err11  "+error)
    }
      
      

  }

  render() {
    console.log('fata = '+JSON.stringify(this.state.data))
    var goto = 'ListContactScreen'
    try {
      if (this.state.data[0].status==='parent'){
        goto = 'ChatScreen'
    }  
    } catch (error) {
      
    }
    return (  
      <View style={styles.container}>
        {/*<TouchableOpacity onPress={()=>{this.props.navigation.navigate(goto,{item:this.state.data})}}>
          <Text>go to contact list</Text>
        </TouchableOpacity>
        
        <Text>login sebagai : </Text>
        <Text>{this.state.data[0].nama}</Text>
        <TouchableOpacity onPress={this.gotoLogin.bind(this)}>
          <Text>{this.state.login}</Text>
    </TouchableOpacity>*/}
    <Modal
          isVisible={this.state.visibleModal}
          animationIn={'slideInLeft'}
          animationOut={'slideOutRight'}
        >
          {this._renderModalContent()}
    </Modal>
    <View>
    <ImageBackground source={require('../assets/background.jpg')} style={{height:200, width:'100%'}}>
        <Text style={{paddingTop: 35, paddingLeft:10, color:'white', fontSize:20, fontStyle:'italic'}}>Selamat Datang </Text>
        <Text style={{paddingLeft: 10, color:'white', fontSize:20, fontStyle:'italic', fontWeight:'bold'}}>{this.state.data[0].nama}</Text>
        <Text style={{paddingLeft: 10, color:'white', fontSize:20, fontStyle:'italic', fontWeight:'bold'}}>{this.state.curTime}</Text>
        <TouchableOpacity onPress={()=>{this.props.navigation.navigate(goto,{item:this.state.data})}} style={[{
          position:'absolute', top:10, right:10}, styles.box]}>
              <Icon name={'chat'} size={40} color="white" />
        </TouchableOpacity>
    </ImageBackground>
    </View>
    
    <View style={styles.searchView}>
          <TouchableOpacity onPress={()=>{this.props.navigation.navigate('MoreTournamentScreen')}}>
            <SearchBar 
                onChangeText  ={(text) => this.setState({search:text})}
                placeholder   ="Cari ..."
                style         ={{height:Scale(35), width:'90%'}}
                editable      ={false}
            />
          </TouchableOpacity>
    </View>
    <FlatList
    data={this.state.siswa}
    onEndReached={console.log('the end of the night')}
    keyExtractor={(item, index) => `key-${index}`}
    renderItem={({item, index}) => {
      return(
      <SiswaCard
        item= {item}
        onPress = {()=>this.props.navigation.navigate('PenilaianScreen',{item})}
        // modal = {(value)=>this.setState({ visibleModal: !this.state.visibleModal })}
        modal = {(data, index)=>this.onAbsen(item)}
        date = {this.state.curTime}
      />)
    }} 
    />

    <TouchableOpacity onPress={()=>this.props.navigation.navigate('LoginScreen')} style={styles.round}>
          <Icon1 name="logout" size={30} color="white" />
    </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  regis: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    height: Scale(36),
  },
  searchView:{
    padding:Scale(10),
    paddingBottom:Scale(7), 
    alignItems:'center',
    flexDirection:'row', 
    justifyContent:'space-between'
},
  box:{
    position:'absolute',
    right:Scale(10),
    bottom:Scale(20),
    borderRadius:20,
    height:Scale(50),
    justifyContent: 'center',
    alignItems: 'center',
    width:Scale(50),
    backgroundColor:'#6C3483',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,

    elevation: 12,
  },
  round: {
    position:'absolute',
    right:Scale(10),
    bottom:Scale(20),
    borderRadius:360,
    height:Scale(50),
    justifyContent: 'center',
    alignItems: 'center',
    width:Scale(50),
    backgroundColor:'#28B463',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,

    elevation: 12,
  },
  modalContent: {
    backgroundColor: 'white',
    padding: 22,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
});
