/* eslint-disable prettier/prettier */
import React from 'react';
import {View, Text,TouchableOpacity, StyleSheet, FlatList } from 'react-native';
import { getParent,getMessages, postMessage } from '../config/services/api';
import Message from '../Messages'
import Compose from '../Compose'
export default class ListContactScreen extends React.Component {
    static navigationOptions = {
        title: 'Contact List qqss',
    };
    state = {
        messages: [],
        arrt:[]
    }
    
    componentDidMount() {
        try {
            this.unsubscribeGetMessages = getParent((snapshot) => {
                console.log('message parent '+JSON.stringify(snapshot.val()))
                
                this.setState({
                    messages: snapshot.val()
                },()=>{
                    const{messages} = this.state
                    var obj = []

                    const fbObject = snapshot.val();
                    const newArr = Object.keys(fbObject).map((key) => {
                    fbObject[key].id = key;
                    return fbObject[key];
                    });

                    newArr.map((data, index) => {
                        console.log("dataaa "+index+" = "+JSON.stringify(data))

                        // if (data.id==="1"){
                        //     obj.push([data.messages])
                        //     console.log("dataaa messf "+JSON.stringify(data.messages))
                        // }
                        
                    })
                    // this.setState({messages:obj[0]},()=>{
                    //     this.setState({messages:this.state.messages[0]},()=>{
                    //         // console.log("hasil "+JSON.stringify(this.state.messages))
                    //         const dataArr = Object.keys(this.state.messages).reduce(
                    //             (arr, key) => arr.concat(this.state.messages[key]),
                    //             []
                    //         )
                    //         this.setState({arrt:dataArr},()=>{
                    //             console.log("hasi  dsl "+JSON.stringify(this.state.arrt))
                    //         })

                    //     })
                    // })
                    
                })
            })    
        } catch (error) {
            console.log("erorrr = "+error)
        }
        
    }
    componentWillUnmount() {
        try {
            // this.unsubscribeGetMessages();    
        } catch (error) {
            
        }
        
    }
    
    GridView1({ item }) {
        try {
            console.log('pesan aku kampang lah = '+JSON.stringify(item))
        } catch (error) {
            
        }
        
        return (
            <View style={{}}>
            <TouchableOpacity onPress={()=>{this.props.navigation.navigate('ChatScreen',{item})}}>
                <Text>{item.nama}</Text>
            </TouchableOpacity>
            </View>
        );
    }
    render() {
        return (
            <View style={styles.container}>
                {<FlatList
                    style={styles.container}
                    data={this.state.messages}
                    renderItem={this.GridView1.bind(this)}
                    keyExtractor={(item, index) => (`message-${index}`)}
                />}
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    message: {
        width: '70%',
        margin: 10,
        padding: 10,
        backgroundColor: 'white',
        borderColor: '#979797',
        borderStyle: 'solid',
        borderWidth: 1,
        borderRadius: 10,
    },
    incomingMessage: {
        alignSelf: 'flex-end',
        backgroundColor: '#E1FFC7',
    },
    listItem: {
        width: '70%',
        margin: 10,
        padding: 10,
        backgroundColor: 'white',
        borderColor: '#979797',
        borderStyle: 'solid',
        borderWidth: 1,
        borderRadius: 10
    },
    
});
