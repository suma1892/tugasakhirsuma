import React from "react";
import Icon from 'react-native-vector-icons/AntDesign';
import { ImageBackground,Image, Text, View,TextInput, StyleSheet,TouchableOpacity, Dimensions } from 'react-native';
import {Navigation} from 'react-native-navigation';
import { goToAuth, goHome } from './navigation'
import { validate } from "@babel/types";
import { Input } from "native-base";
import md5 from 'md5';

export default class Login extends React.Component {

  
  constructor(){
    super();
    this.state = {
      User : {
        phoneNumber: '',
        password: '',
        date : new Date().getDate(),
        IMEI : 0
      }
    }
  }

  setState(){
    this.props.inputField;
  }

  static get options() {
    return {
      topBar: {
         visible: false,
         height: 0,
      }
    };
  }

  openpage(){
    Navigation.push(this.props.componentId, {
      component: {
        name: 'Login',
      }
    })
  }

  action() {
    var i = 0
    var status = true
    while (i < Object.keys(this.state.User).length) {
        this.setState({ [Object.keys(this.state.User)[i] + '_error']: null })
        if (!this.state.User[Object.keys(this.state.User)[i]]) {
            status = false
            this.setState({ [Object.keys(this.state.User)[i] + '_error']: 'tidak boleh kosong' })
        }
        i++
    }
    
    if (status) {
        this.getLogin()
    }
  }

  getLogin(){
    console.log('data = '+JSON.stringify(this.state.User))
  }

  componentDidMount(){
  //kosong  
  }
  render() {
    return (
      <View style={{flex:1}}>
      <ImageBackground resizeMode="stretch" source={require('./img/background-login.png')} style={{flex: 1}}>
         <View style={{flex:1,justifyContent: 'center',marginHorizontal:40}}>
              <Text style={styles.top_1}>Selamat Datang suma</Text>
              <Text style={{fontSize: 15,color: '#000',fontFamily:'ProximaNova-Regular',marginTop:5}}>Proses Login Anda</Text>
              <View style={{marginTop:10}}>
               <Text style={{fontSize: 15,color: '#000',fontFamily:'ProximaNova-Regular',marginVertical:10}}>No. Telephone</Text>
               <TextInput
                 style={styles.inputField}
                // onChangeText={(text)=>this.validate(text)}
                 underlineColorAndroid="transparent"
                 onChangeText={data => this.setState(prevState => ({
                              User: {
                                  ...prevState.User,
                                  phoneNumber: data
                              }
                          }))}
               />
            </View>

            <View>
              <Text style={{fontSize: 15,color: '#000',fontFamily:'ProximaNova-Regular',marginVertical:10}}>
              {this.state.User.password == '' ? 'Password' : md5(this.state.User.password)}
              </Text>
              <TextInput
                style={styles.inputField}
                underlineColorAndroid="transparent"
                //onChangeText={password => this.setState({password})}
                onChangeText={data => this.setState(prevState => ({
                              User: {
                                  ...prevState.User,
                                  password: data
                              }
                          }))}
                //value={this.state.User.password}
              />
            </View>

           <TouchableOpacity onPress={() => this.action()} style={{backgroundColor:'rgb(196, 2, 2)',height:50,marginTop:25,borderRadius:60/2,justifyContent: 'center',alignItems: 'center'}}>
             <Text style={{fontSize: 15,color: '#fff',fontFamily:'ProximaNova-Regular',textAlign: 'center',}}>Submit</Text>
           </TouchableOpacity>

           <Text  onPress={() => {
             Navigation.push(this.props.componentId, {
               component: {
                 name: 'Lupa_password',
                 passProps: {
                 }
               }
             })
           }} style={{fontSize: 15,color: '#000',fontFamily:'ProximaNova-Bold',textAlign: 'center',marginTop:25}}>Forgot Password</Text>
         </View>
      </ImageBackground>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  inputField :{
    backgroundColor:'#dadadab5',
    fontSize: 13,
    color:'#000',
    paddingVertical:3, 
    fontFamily:'ProximaNova-Regular'
  },
  error:{
    borderWidth:3,
    borderColor:'red'
  },

  top_1:{
    fontSize: 15,
    color: '#000',
    fontFamily:'ProximaNova-Bold',
    marginTop:110
  }
})