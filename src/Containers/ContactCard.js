/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
import {
Text,
View,
StyleSheet,
TouchableNativeFeedback,
Image,
TouchableWithoutFeedback,
TouchableOpacity,
} from 'react-native';
import {getImage, Scale} from '../Assets/index';
import FastImage from 'react-native-fast-image';

export class ContactCard extends Component {
constructor() {
    super();
    this.state = {
    value: '',
    };
}
_isChar = value => {
    // if (/^\d+$/.test(value))
    this.props.onChangeText(value);
};
render() {
    const {item, onPress} = this.props;
    try {
    var img = getImage(item.banner);
    var lg = getImage(item.logo.logo_original);
    } catch (error) {}
    return (
    <TouchableWithoutFeedback onPress={onPress}>
        <View style={s.slideTurnament}>
            
        </View>
    </TouchableWithoutFeedback>
    );
}
}
const s = StyleSheet.create({
img: {
    width: Scale(200),
    height: Scale(130),
    borderRadius: 20,
},
boxname: {
    paddingLeft: Scale(10),
    paddingTop: Scale(10),
    paddingRight: Scale(10),
    width: Scale(140),
    justifyContent: 'space-between',
},
regis: {
    backgroundColor: 'orange',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    height: Scale(36),
    width: Scale(100),
},
inputContainer: {
    borderRadius: 20,
    paddingLeft: 5,
    borderWidth: 2,
    backgroundColor: '#e6e6e6',
},
ava: {
    flexDirection: 'row',
    paddingBottom: Scale(10),
    paddingLeft: Scale(10),
    paddingTop: Scale(5),
    paddingRight: Scale(10),
    justifyContent: 'space-between',
},
slideTurnament: {
    flexDirection: 'column',
    borderRadius: 20,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
    width: 0,
    height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
    margin: 10,
},
});
