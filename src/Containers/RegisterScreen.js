/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
import {
ActivityIndicator,
TouchableOpacity,
Alert,
Platform,
StyleSheet,
AsyncStorage,
Text,
Button,
View,
Dimensions,
} from 'react-native';
import {Input} from '../Components/InputCardComponent';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import { getData,getParent, postMessage, postRegisterTeacher } from '../config/services/api';

const Scale = value => {
const {width} = Dimensions.get('window');
const guidelineBaseWidth = 350;

return (width / guidelineBaseWidth) * value;
};

// eslint-disable-next-line prettier/prettier
var radio_props = [
{label: 'Guru   ', value: 0},
{label: 'Orang tua', value: 1},
];
export default class RegisterScreen extends Component {
constructor(props) {
    super(props);
    this.state = {
    teacher: {
        username: null,
        password: null,
        repassword:null,
        status: 'teacher',
    },
    };
}

static navigationOptions = ({navigation}) => ({
    title: 'Register',
    headerRight: (
    <TouchableOpacity onPress={() => navigation.navigate('LoginScreen')}>
        <Text style={{color: 'orange', paddingRight: 10}}>Login</Text>
    </TouchableOpacity>
    ),
    headerStyle: {
    backgroundColor: 'white',
    },
    headerTintColor: 'black',
    headerTitleStyle: {
    fontWeight: 'bold',
    },
});

componentDidMount() {
    
}



register() {
    console.log('etacher = '+JSON.stringify(this.state.teacher))
    var i = 0
    // var status1 = true
    //     while (i < Object.keys(this.state.teacher).length) {

    //         this.setState({ [Object.keys(this.state.teacher)[i] + '_error']: null })
    //         if (!this.state.teacher[Object.keys(this.state.teacher)[i]]) {
    //             status1 = false
    //             this.setState({ [Object.keys(this.state.teacher)[i] + '_error']: "harus diisi" })
    //         }
    //         i++
    //     }
    // if (status1){
    //     this.unsubscribeGetMessages = getData(radio_props===0?'teacher':'parent',(snapshot) => {
    //         var tamp = []
    //         tamp = snapshot.val()
    //         console.log('snapsot = '+JSON.stringify(snapshot.val()))
    //         var data = postRegisterTeacher(this.state.teacher.username,this.state.teacher.password, 'teacher', tamp.length)
    //         console.log('darta = '+JSON.stringify(data))
//     //     })

//     }
// } catch (error) {
//     console.log("erorrr = "+error)
}



render() {
    return (
    <View style={styles.container}>
        {
        <View>
        <Text style={{paddingBottom:10}}>Register Sebagai :</Text>
            <RadioForm
                radio_props={radio_props}
                initial={0}
                onPress={value => {
                    this.setState(prevState => ({
                        teacher: {
                            ...prevState.teacher,
                            status: value===0?'teacher':'parent'
                        },
                        }))
            }}
            formHorizontal={true}
            />
            <Input
                label="Email"
                value={this.state.teacher.username}
                onChangeText={text =>
                    this.setState(prevState => ({
                    teacher: {
                        ...prevState.teacher,
                        username: text,
                    },
                    }))
                }
                statusError={false}
                pdg={0}
                error         ={this.state.username_error}
                borderColor   ={this.state.username_error?'red':'white'}
            />
            <Input
                label="Password"
                onChangeText={text =>
                    this.setState(prevState => ({
                    teacher: {
                        ...prevState.teacher,
                        password: text,
                    },
                    }))
                }
                value={this.state.teacher.password}
                statusError={false}
                pdg={0}
                secureTextEntry={this.state.secure}
                style={{width: Scale(280)}}
                status={true}
                onChangeEye={() => this.setState({secure: !this.state.secure})}
                icon={this.state.secure ? 'ios-eye' : 'ios-eye-off'}
                error         ={this.state.password_error}
                borderColor   ={this.state.password_error?'red':'white'}
                />

                <Input
                label="Re Password"
                onChangeText={text =>
                    this.setState(prevState => ({
                    teacher: {
                        ...prevState.teacher,
                        repassword: text,
                    },
                    }))
                }
                value={this.state.teacher.repassword}
                statusError={false}
                pdg={0}
                secureTextEntry={this.state.secure}
                style={{width: Scale(280)}}
                status={true}
                onChangeEye={() => this.setState({secure: !this.state.secure})}
                icon={this.state.secure ? 'ios-eye' : 'ios-eye-off'}
                error         ={this.state.repassword_error}
                borderColor   ={this.state.repassword_error?'red':'white'}
                />
            <Button
                onPress={() => this.register()}
                title={'Register'}
                color="#841584"
                accessibilityLabel="Login"
            />
        </View>
        }
    </View>
    );
}
}

const styles = StyleSheet.create({
container: {
    flex: 1,
    padding: 15,
    backgroundColor: 'white',
},
});
