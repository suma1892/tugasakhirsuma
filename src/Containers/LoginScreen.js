/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
import {
ActivityIndicator,
TouchableOpacity,
Alert,
Platform,
StyleSheet,
AsyncStorage,
Text,
Button,
View,
Dimensions,
} from 'react-native';
import {Input} from '../Components/InputCardComponent';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import { getData,getParent, postMessage } from '../config/services/api';

const Scale = value => {
const {width} = Dimensions.get('window');
const guidelineBaseWidth = 350;

return (width / guidelineBaseWidth) * value;
};

// eslint-disable-next-line prettier/prettier
var radio_props = [
{label: 'Guru   ', value: 0},
{label: 'Orang tua', value: 1},
];
export default class LoginScreen extends Component {
constructor(props) {
    super(props);
    this.state = {
    Users: {
        username: null,
        password: null,
        status: 'teacher',
    },
    };
}

static navigationOptions = ({navigation}) => ({
    title: 'Login',
    headerRight: (
    <TouchableOpacity onPress={() => navigation.navigate('RegisterScreen')}>
        <Text style={{color: 'orange', paddingRight: 10}}>Register</Text>
    </TouchableOpacity>
    ),
    headerStyle: {
    backgroundColor: 'white',
    },
    headerTintColor: 'black',
    headerTitleStyle: {
    fontWeight: 'bold',
    },
});

componentDidMount() {}

Login() {
    var i = 0
    var status1 = true
        while (i < Object.keys(this.state.Users).length) {

            this.setState({ [Object.keys(this.state.Users)[i] + '_error']: null })
            if (!this.state.Users[Object.keys(this.state.Users)[i]]) {
                status1 = false
                this.setState({ [Object.keys(this.state.Users)[i] + '_error']: "harus diisi" })
            }
            i++
        }
    if (status1)
    this.unsubscribeGetMessages = getData(this.state.Users.status,(snapshot) => {
        console.log('message atas = '+JSON.stringify(snapshot.val()))
        var res = []
        const fbObject = snapshot.val();
        const newArr = Object.keys(fbObject).map((key) => {
        fbObject[key].id = key;
        return fbObject[key];
        });
        res = snapshot.val()
        console.log('Users = '+JSON.stringify(newArr))
        var obj=[]
        let stat = false
        try {
            newArr.map((data, index)=>{
                console.log('data ss= '+JSON.stringify(data))
                if(
                    data.nama === this.state.Users.username && 
                    data.password === this.state.Users.password && 
                    data.status===this.state.Users.status){
                    obj.push(data)
                    stat = true
                }
            })
        } catch (error) {
            console.log(error)
        }
        
        try {
            if(stat === true){
                console.log('masuk pa eko = '+JSON.stringify(obj))
                AsyncStorage.setItem('Users', JSON.stringify(obj));
                this.props.navigation.navigate('HomeScreen')

            }else{
                Alert.alert('Email atau Password salah, silahkan coba lagi !!')
            }    
        } catch (error) {
            
        }
        

    })    
} catch (error) {
    console.log("erorrr = "+error)
}



render() {
    return (
    <View style={styles.container}>
        {
        <View>
        <Text style={{paddingBottom:10}}>Login Sebagai :</Text>
            <RadioForm
                radio_props={radio_props}
                initial={0}
                onPress={value => {
                    this.setState(prevState => ({
                        Users: {
                            ...prevState.Users,
                            status: value===0?'teacher':'parent'
                        },
                        }))
            }}
            formHorizontal={true}
            />
            <Input
                label="Email"
                value={this.state.Users.username}
                onChangeText={text =>
                    this.setState(prevState => ({
                    Users: {
                        ...prevState.Users,
                        username: text,
                    },
                    }))
                }
                statusError={false}
                pdg={0}
                error         ={this.state.username_error}
                borderColor   ={this.state.username_error?'red':'white'}
            />
            <Input
                label="Password"
                onChangeText={text =>
                    this.setState(prevState => ({
                    Users: {
                        ...prevState.Users,
                        password: text,
                    },
                    }))
                }
                value={this.state.Users.password}
                statusError={false}
                pdg={0}
                secureTextEntry={this.state.secure}
                style={{width: Scale(280)}}
                status={true}
                onChangeEye={() => this.setState({secure: !this.state.secure})}
                icon={this.state.secure ? 'ios-eye' : 'ios-eye-off'}
                error         ={this.state.password_error}
                borderColor   ={this.state.password_error?'red':'white'}
                />
            <Button
                onPress={() => this.Login()}
                title={'Login'}
                color="#841584"
                accessibilityLabel="Login"
            />
        </View>
        }
    </View>
    );
}
}

const styles = StyleSheet.create({
container: {
    flex: 1,
    padding: 15,
    backgroundColor: 'white',
},
});
