/* eslint-disable prettier/prettier */
import React from 'react';
import Home from '../Containers/HomeScreen';
import ChatScreen from '../Containers/ChatScreen';
import ListContactScreen from '../Containers/ListContactScreen'
import LoginScreen from '../Containers/LoginScreen'
import RegisterScreen from '../Containers/RegisterScreen'
import FirstScreen from '../Containers/FirstScreen'
import PenilaianScreen from '../Containers/PenilaianScreen'
const routes = {

  HomeScreen: {screen: Home},

  PenilaianScreen:{screen:PenilaianScreen},
  
  FirstScreen:{screen:FirstScreen},

  
  LoginScreen:{screen:LoginScreen},

  ChatScreen: {screen: ChatScreen},

  RegisterScreen:{screen:RegisterScreen},
  
  ListContactScreen: {screen:ListContactScreen},
  
};
export default routes;
