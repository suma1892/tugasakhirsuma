/* eslint-disable prettier/prettier */
import { setListener,pushData, initialize } from './firebase';
export const initApi = () => initialize();
import firebase from 'firebase';

// eslint-disable-next-line no-undef
export const getMessages = (updaterFn) => {
    setListener('messages', updaterFn)
};


export const getData = (orang, updaterFn) => setListener(orang, updaterFn)

export const getSiswa = (updaterFn) => setListener('siswa', updaterFn);

export const getParent = (updaterFn) => setListener('parent', updaterFn);

export const getTeacher = (updaterFn) => setListener('teacher', updaterFn);

export const postMessage = (message, parentID, teacherID, status) => {
    firebase.database().ref('messages').push({
        chat: message,
        parentID:parentID,
        teacherID:teacherID,
        status:status
    
    }).then((data)=>{
        //success callback
        console.log('data adalah ' , JSON.stringify(data))
    }).catch((error)=>{
        //error callback
        console.log('error ' , error)
    })
}

export const postMessageParent = (message, parentID, teacherID, status) => {
    firebase.database().ref('messages').push({
        chat: message,
        parentID:parentID,
        teacherID:teacherID,
        status:status
    
    }).then((data)=>{
        //success callback
        console.log('data adalah ' , JSON.stringify(data))
    }).catch((error)=>{
        //error callback
        console.log('error ' , error)
    })
}


export const postRegisterTeacher = ( nama, password, status, teacherID, ) => {
    firebase.database().ref('teacher').push({

        teacherID:teacherID,
        status:status,
        nama:nama,
        password:password
    
    }).then((data)=>{
        //success callback
        console.log('data adalah ' , JSON.stringify(data))
        return data
    }).catch((error)=>{
        //error callback
        console.log('error ' , error)
    })
}


export const postAbsen = ( id, keterangan, tanggal ) => {
    firebase.database().ref('student/'+id+'/absen').push({

        keterangan:keterangan,
        tanggal:tanggal
    
    }).then((data)=>{
        //success callback
        console.log('data adalah ' , JSON.stringify(data))
        return data
    }).catch((error)=>{
        //error callback
        console.log('error ' , error)
    })
}

export const updateAbsen = ( idParent, idChild, keterangan ) => {
    
    firebase.database().ref('student/' + idParent+'/absen/'+idChild).update({'keterangan': keterangan}).then((data)=>{
        //success callback
        console.log('data adalah update ' , JSON.stringify(data))
        return data
    }).catch((error)=>{
        //error callback
        console.log('error ' , error)
    })
    
}
