/* eslint-disable no-const-assign */
import * as firebase from 'firebase';

const config = {
  apiKey: 'AIzaSyBxpYIE6Z8W3Gc3pWCNM7uQR0hmolQEQX0',
  authDomain: 'tugasakhirsuma.firebaseapp.com',
  databaseURL: 'https://tugasakhirsuma.firebaseio.com',
  projectId: 'tugasakhirsuma',
  storageBucket: 'tugasakhirsuma.appspot.com',
  messagingSenderId: '1021270976118',
  appId: '1:1021270976118:web:1fd63350f72d9076372157',
};

export const initialize = () =>
  !firebase.apps.length ? firebase.initializeApp(config) : firebase.app();


export const setListener = (endpoint, updaterFn) => {
  firebase
    .database()
    .ref(endpoint)
    .on('value', updaterFn);
  return () =>
    firebase
      .database()
      .ref(endpoint)
      .off();
};

export const pushData = (endpoint, data) => {
  return firebase
    .database()
    .ref(endpoint)
    .push(data);
};
