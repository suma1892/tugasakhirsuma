/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  Alert,
} from 'react-native';
import { Tooltip } from 'react-native-elements';

import {Scale} from '../assets/index';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
// import {Avatar, Button} from 'react-native-elements';
// import FastImage from 'react-native-fast-image'

export class SiswaCard extends Component {
  constructor() {
    super();
    this.state = {
      value: '',
    };
  }
  _isChar = value => {
    // if (/^\d+$/.test(value))
    this.props.onChangeText(value);
  };

  changeAbsen(){
    // console.log('masuk pa ejkooooo')
    // Alert.alert('ganti absen')
  }
  render() {
    const {item} = this.props;
    // console.log("lastdate = "+item.lastAbsen)
    // console.log("date = "+this.props.date)
    const fbObject = item.absen;
    var newArr = []
    try {
      const fbObject = item.absen;
        newArr = Object.keys(fbObject).map((key) => {
        fbObject[key].id = key;
        return fbObject[key];
        });  
    } catch (error) {
      
    }
    // console.log('newArr = '+JSON.stringify(newArr))
    var tamp =[]
    newArr.map((data, index)=>{
        tamp = data
    })
    // console.log('arr tamp =s '+JSON.stringify(tamp))
    return (
      <View style={s.slideTurnament}>
        <View>
          <View style={{flexDirection: 'row'}}>
            <View>
              <Image
                style={s.img}
                source={{
                  uri:
                    'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRe2MdwHaI53vCE_L78LyMCQz6MlKhAAGtZnM21eCpHbWvmzewE',
                }}
                resizeMode="stretch"
              />
            </View>

            <View style={s.boxname}>
              <Text style={{fontWeight: 'bold', fontSize: 13}}>
                Nama : {item.nama}
              </Text>
              <Text style={{fontWeight: 'bold', fontSize: 13}}>
                Umur : {item.umur} Tahun
              </Text>
              <Text style={{fontWeight: 'bold', fontSize: 13}}>
                Hafalan : {item.hafalan}
              </Text>

              <View style={{padding: Scale(10), width: '90%'}} />
              <View
                style={{
                  flexDirection: 'row',
                  padding: 5,
                  justifyContent: 'space-between',
                }}>
                <TouchableOpacity style={s.regis} onPress={this.props.onPress}>
                  <Text style={{color: 'white'}}>Nilai</Text>
                </TouchableOpacity>
                <View style={{width: 10}} />
                {tamp.tanggal!==this.props.date?<TouchableOpacity
                  style={s.regis}
                  onPress={ this.props.modal}>
                  <Text style={{color: 'white'}}>Absen</Text>
                </TouchableOpacity>:
                <TouchableOpacity
                  style={[s.regis,{backgroundColor:tamp.keterangan=='hadir'?'green':tamp.keterangan=='izin'?'blue':
                  tamp.keterangan=='alfa'?'red':tamp.keterangan=='sakit'?'yellow':'grey'}]}
                  onPress={ this.props.modal } >
                  <Text style={{color: 'white'}}>{tamp.keterangan}</Text>
                  
                </TouchableOpacity>}
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
const s = StyleSheet.create({
  img: {
    width: Scale(150),
    height: Scale(130),
    borderRadius: 20,
  },
  regis: {
    backgroundColor: 'orange',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    height: Scale(36),
    width: Scale(70),
  },
  boxname: {
    paddingLeft: Scale(15),
    paddingTop: Scale(10),
    paddingRight: Scale(10),
    width: Scale(160),
    justifyContent: 'space-between',
  },
  inputContainer: {
    borderRadius: 20,
    paddingLeft: 5,
    borderWidth: 2,
    backgroundColor: '#e6e6e6',
  },
  ava: {
    flexDirection: 'row',
    paddingBottom: Scale(10),
    paddingLeft: Scale(10),
    paddingTop: Scale(5),
    paddingRight: Scale(10),
    justifyContent: 'space-between',
  },
  slideTurnament: {
    flexDirection: 'column',
    borderRadius: 20,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
    margin: 10,
  },
});
