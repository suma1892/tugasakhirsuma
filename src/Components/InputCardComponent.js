import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
export class Input extends Component {
  constructor() {
    super();
    this.state = {
      value: '',
      visibleModal: false,
    };
  }
  _isChar = value => {
    // if (/^\d+$/.test(value))
    this.props.onChangeText(value);
  };
  render() {
    const {
      label,
      value,
      error,
      statusError,
      borderColor,
      keyboardType,
      maxLength,
      secureTextEntry,
      pdg,
      numberOfLines,
      multiline,
      placeholder,
      style,
      editable,
      autoFocus,
      status,
      icon,
      onChangeEye,
    } = this.props;
    return (
      <View style={{paddingTop: pdg}}>
        {label && <Text style={{color: '#a3a3a3'}}>{label}</Text>}
        <View
          style={[
            s.inputContainer,
            {
              borderColor: borderColor,
              justifyContent: 'space-between',
              flexDirection: status ? 'row' : 'column',
            },
          ]}>
          <TextInput
            value={value}
            maxLength={maxLength}
            secureTextEntry={secureTextEntry}
            keyboardType={keyboardType}
            onChangeText={e => {
              this._isChar(e);
            }}
            numberOfLines={numberOfLines}
            multiline={multiline}
            placeholder={placeholder}
            style={[{height: 40}, style]}
            editable={editable}
            autoFocus={autoFocus}
          />
          {status && (
            <View style={{paddingRight: 10, alignSelf: 'center'}}>
              <TouchableOpacity onPress={onChangeEye}>
                <Icon name={icon} size={25} color="#c6c6c6" />
              </TouchableOpacity>
            </View>
          )}
        </View>
        {error === ''
          ? false
          : true && <Text style={{color: 'red'}}>{error}</Text>}
      </View>
    );
  }
}
const s = StyleSheet.create({
  inputContainer: {
    borderRadius: 5,
    paddingLeft: 5,
    borderWidth: 2,
    backgroundColor: '#e6e6e6',
  },
});
