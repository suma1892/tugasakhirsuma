/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
import {Text, View, StyleSheet, TextInput} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';

export class SearchBar extends Component {
constructor() {
super();
this.state = {
    value: '',
};
}
_isChar = value => {
// if (/^\d+$/.test(value))
this.props.onChangeText(value);
};
render() {
const {
    value,
    keyboardType,
    maxLength,
    placeholder,
    style,
    editable,
} = this.props;

return (
    <View>
    <View style={[s.inputContainer, {flexDirection: 'row'}]}>
        <TextInput
            value={value}
            maxLength={maxLength}
            keyboardType={keyboardType}
            onChangeText={e => {
                this._isChar(e);
            }}
            placeholder={placeholder}
            style={style}
            editable={editable}
        />
        <View style={{paddingRight: 10}}>
        <Icon name="search1" size={15} color="#c6c6c6" />
        </View>
    </View>
    </View>
);
}
}
const s = StyleSheet.create({
inputContainer: {
borderRadius: 50,
paddingLeft: 5,
backgroundColor: '#e6e6e6',
alignItems: 'center',
},
});
