import {Platform, Dimensions, StyleSheet, StatusBar} from 'react-native';

const {width, height} = Dimensions.get('window');
const screenWidth = width < height ? width : height;
const screenHeight = width < height ? height : width;



const Colors = {
  putihapa: '#F1FFFF',
  transparent: 'transparent',
  warm_grey: '#888888',
  white: '#ffffff',
  black: '#212121',
  gray: '#606060',
  darkgray: '#767676',
  blue: '#193f79',
  secondary: 'rgba(44,45,45,1)',
  whitesmoke: 'rgba(245, 245, 245,1)',
  overlay: 'rgba(0,0,0,0.5)',
  tangerine: '#fe8800',
  background: 'rgba(255,255,255,1)',
  pastel_red: '#F04134',
  benam: '#6b7c93',
  boldAvenir: '#222222',
  borderColor: '#dddddd',

  red: '#f91732',
  apple: '#66B445',
  blumine: '#204A8B',
  pizzaz: '#FE8800',
  border: '#DDDDDD',
  slate_gray: '#6B7C93',
  concrete: '#F2F2F2',
  yellow: '#fff8d2',
};

const Scale = value => {
  const {width} = Dimensions.get('window');
  const guidelineBaseWidth = 350;

  return (width / guidelineBaseWidth) * value;
};
const Metrics = {
  navBot: Scale(50),
  screenWidth,
  screenHeight,
  normalPadding: Scale(16),
  baseMargin: Scale(10),
  sizePad: Scale(12),
  sizeList: Scale(80),
  headerModal: Scale(Platform.OS === 'ios' ? 44 : 54),
  navBarHeight: Scale(Platform.OS === 'ios' ? 64 : 56),
  statusBar: StatusBar.currentHeight,
  font: {
    tiny: 12,
    small: 14,
    regular: 16,
    medium: 20,
    large: 24,
    xl: 28,
    screenWidth,
    screenHeight,
  },

  padding: {
    tiny: Scale(4),
    small: Scale(8),
    normal: Scale(16),
    medium: Scale(24),
    semi: Scale(20),
    large: Scale(32),
  },

  icon: {
    tiny: Scale(12),
    small: Scale(18),
    normalSmall: Scale(21),
    normal: Scale(32),
    medium: Scale(40),
    large: Scale(48),
  },

  screen: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    scale: Dimensions.get('window').scale,
  },

  toolbar: 60,
  navbar: 60,
  border: StyleSheet.hairlineWidth,
};

const Fonts = {
  light: {
    fontFamily: Platform.OS === 'ios' ? 'Avenir Next' : 'AvenirNextLTProMedium',
    fontWeight: Platform.OS === 'ios' ? '300' : undefined,
  },
  regular: {
    fontFamily: Platform.OS === 'ios' ? 'Avenir Next' : 'AvenirNextLTProMedium',
    fontWeight: Platform.OS === 'ios' ? '500' : undefined,
  },
  bold: {
    fontFamily: Platform.OS === 'ios' ? 'Avenir Next' : 'AvenirNextLTProDemi',
    fontWeight: Platform.OS === 'ios' ? '600' : undefined,
  },
  size: {
    h1: 38,
    h2: 34,
    h5: 20,
    tiny: Scale(10),
    small: Scale(11.5),
    regular: Scale(13.5),
    medium: Scale(15),
    large: Scale(17.5),
    xtra: Scale(25),
  },
};

const Style = {
  heading: {
    ...Fonts.regular,
    fontSize: Metrics.font.small,
  },
  propBold: {
    ...Fonts.bold,
    fontSize: Metrics.font.large,
  },
  entity: {
    ...Fonts.regular,
    color: Colors.darkgray,
    fontSize: Metrics.font.small,
  },
  value: {
    ...Fonts.regular,
    fontSize: Metrics.font.regular,
    color: Colors.gray,
  },
  description: {
    ...Fonts.regular,
    fontSize: Metrics.font.regular,
  },
  typeCluster: {
    ...Fonts.light,
    fontSize: Metrics.font.small,
  },
  typeUnit: {
    ...Fonts.regular,
    fontSize: Metrics.font.regular,
  },
  priceUnit: {
    ...Fonts.regular,
    fontSize: Metrics.font.regular,
  },
  amount: {
    ...Fonts.bold,
    // eslint-disable-next-line prettier/prettier
        fontSize: Metrics.font.regular,
  },
};

export {Fonts, Metrics, Colors, Style, Scale};
